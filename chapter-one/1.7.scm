#lang sicp

; on small numbers naive good-enough? is relatively large
;   compared to the value of the square root
; for large numbers the epsilon may be smaller
;   than floating point values at that scale may allow

(define (square x) (* x x))
(define (average a b) (/ (+ a b) 2))

(define epsilon-portion 0.000001)
(define (good-enough? guess x)
	(< (/ (abs (- (improve guess x) guess)) guess) epsilon-portion)
)

(define (improve guess x)
	(average guess (/ x guess))
)

(define (sqrt-iter guess x)
	(if (good-enough? guess x)
		guess
		(sqrt-iter (improve guess x) x)
	)
)
(define (sqrt x) (sqrt-iter 1.0 x))



(sqrt 2.0)
(sqrt 0.00000002)
(sqrt 20000000000)
