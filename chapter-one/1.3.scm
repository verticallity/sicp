#lang sicp

(define (square x) (* x x))

; idk this is a classic one you can make more simple
; just start off with lazy way
(define (sum-greatest-two-squares a b c)
	(cond
		((and (<= c a) (<= c b)) (+ (square a) (square b)))
		((and (<= b a) (<= b c)) (+ (square a) (square c)))
		(else (+ (square b) (square c)))
	)
)

(sum-greatest-two-squares 2 3 4)
;> 25
(sum-greatest-two-squares 4 2 3)
;> 25
(sum-greatest-two-squares 3 4 2)
;> 25
(sum-greatest-two-squares 1 1 1)
;> 2
