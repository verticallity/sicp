#lang sicp

; prediction: does a + |b|, replacing the operator depending on conditional
(define (a-plus-abs-b a b)
	((if (> b 0) + -) a b)
)

(a-plus-abs-b 3 2)
;> 5
(a-plus-abs-b 3 -2)
;> 5
