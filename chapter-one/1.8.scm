#lang sicp

(define epsilon-portion 0.000001)
(define (good-enough? guess x)
	(< (/ (abs (- (improve guess x) guess)) guess) epsilon-portion)
)

(define (improve guess x)
	(/ (+ (/ x guess guess) guess guess) 3)
)

(define (cbrt-iter guess x)
	(if (good-enough? guess x)
		guess
		(cbrt-iter (improve guess x) x)
	)
)
(define (cbrt x) (cbrt-iter 1.0 x))

(cbrt 2.0)
(cbrt 2000000.0)
(cbrt 0.000002)
