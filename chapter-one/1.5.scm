#lang sicp

(define (p) (p))
(define (test x y)
	(if (= x 0) 0 y)
)

; prediction: in normal-order this does not ever evaluate
; in applicative-order it does since (if ...) is replaced with the first value
(test 0 (p))
; okay i was wrong!
; if is not treated as special, right, so no shortcircuiting
; or i guess shortcircuiting but the inputs have to actually resolve
; bc both are still substituted and that loops
; hmm
