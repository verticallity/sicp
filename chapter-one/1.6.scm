#lang sicp

(define (square x) (* x x))

(define (average a b) (/ (+ a b) 2))

(define epsilon 0.001)
(define (good-enough? guess x)
	(< (abs (- (square guess) x)) epsilon)
)

(define (improve guess x)
	(average guess (/ x guess))
)

(define (alt-if pred on-true on-false)
	(cond (pred on-true) (else on-false))
)

(define (sqrt-iter guess x)
	(alt-if (good-enough? guess x)
		guess
		(sqrt-iter (improve guess x) x)
	)
)
(define (sqrt x) (sqrt-iter 1.0 x))


; unsure what happens in this example with the alternate if declaration...
; so just gonna try running it ig

(sqrt 2.0)

; okay so it loops without if being special
; i guess bc it does in fact do shortcircuiting
; which is required or else you couldn't do any conditional recursion
